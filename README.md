Packer Antigen Plugin
---------------------

This will install the [packer cli](https://www.packer.io/intro/index.html) on your system using the antigen/antibody plugin manager for zsh.

### Antigen Instructions

Install [antigen](http://antigen.sharats.me/#installation) and configure it in your zshrc.
Add the following line to your zshrc:

```
antigen bundle https://gitlab.com/matthewfranglen/packer-antigen
```

### Antibody Instructions

Install [antibody](https://getantibody.github.io/install/) and configure it in your zshrc.
Add the following line to your zsh-plugins:

```
https://gitlab.com/matthewfranglen/packer-antigen
```
