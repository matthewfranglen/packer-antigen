(
    cd "$(dirname "${0}")"
    if [ ! -e "packer" ]; then
        wget https://releases.hashicorp.com/packer/1.5.5/packer_1.5.5_linux_amd64.zip -O packer.zip
        unzip packer.zip
        rm packer.zip
    fi
)

export PATH=${PATH}:$(dirname $0)
